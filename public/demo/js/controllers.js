var app = angular.module('project', ['ngRoute']);

app.config(function($routeProvider) {
    $routeProvider
        .when('/', { controller:'TodoCtrl', templateUrl:'views/list.html' })
        .when('/archive', { controller:'ArchiveCtrl', templateUrl:'views/list.html' })
        .when('/new', { controller:'DetailsCtrl', templateUrl:'views/details.html' })
        .otherwise({ redirectTo:'/' });
});

app.filter('greet', function() {
    return function(name) {
        return 'Hello, ' + name + '!';
    };
});

app.factory('Storage', function() {
    this.data = {
        'todos': [
            {text: 'learn angular', done: true},
            {text: 'build an angular app', done: false}
        ]
    };
    var $this = this;

    return {
        get: function(key) {
            return $this.data[key];
        },
        set: function(key, value) {
            $this.data[key] = value;
        }
    };

});

app .directive('tabs', function() {
    return {
        restrict: 'E',
        transclude: true,
        scope: {},
        controller: function($scope, $element) {
            var panes = $scope.panes = [];

            $scope.select = function(pane) {
                angular.forEach(panes, function(pane) {
                    pane.selected = false;
                });
                pane.selected = true;
            }

            this.addPane = function(pane) {
                if (panes.length == 0) $scope.select(pane);
                panes.push(pane);
            }
        },
        template:
            '<div class="tabbable">' +
            '<ul class="nav nav-tabs">' +
            '<li ng-repeat="pane in panes" ng-class="{active:pane.selected}">'+
            '<a href="" ng-click="select(pane)">{{pane.title}}</a>' +
            '</li>' +
            '</ul>' +
            '<div class="tab-content" ng-transclude></div>' +
            '</div>',
        replace: true
    };
});

app.directive('pane', function() {
    return {
        require: '^tabs',
        restrict: 'E',
        transclude: true,
        scope: { title: '@' },
        link: function(scope, element, attrs, tabsCtrl) {
            tabsCtrl.addPane(scope);
        },
        template:
            '<div class="tab-pane" ng-class="{active: selected}" ng-transclude>' +
            '</div>',
        replace: true
    };
});

function TodoCtrl($scope, Storage) {
    $scope.todos = Storage.get('todos');

    $scope.remaining = function() {
        var count = 0;
        angular.forEach($scope.todos, function(todo) {
            count += todo.done ? 0 : 1;
        });
        return count;
    };

    $scope.archive = function() {
        var oldTodos = $scope.todos;
        var toArchive = Storage.get('archived') || [];
        $scope.todos = [];
        angular.forEach(oldTodos, function(todo) {
            if (!todo.done) {
                $scope.todos.push(todo);
            }
            else {
                toArchive.push(todo);
            }
        });

        Storage.set('archived', toArchive);
        Storage.set('todos', $scope.todos);
    };

    $scope.$on('$destroy', function() {
        Storage.set('todos', $scope.todos);
    });
}


function DetailsCtrl($scope, Storage) {
    $scope.todos = Storage.get('todos');

    $scope.addTodo = function() {
        $scope.todos.push({text:$scope.todoText, done:false});
        $scope.todoText = '';
    };

    $scope.$on('$destroy', function() {
        Storage.set('todos', $scope.todos);
    });
}

function ArchiveCtrl($scope, Storage) {
    $scope.archived = true;
    $scope.todos = Storage.get('archived') || [];
}